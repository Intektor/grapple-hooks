package de.intektor.mods.grapple_hooks.caps

import io.netty.buffer.ByteBuf
import net.minecraft.client.Minecraft
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.fml.DistExecutor
import net.minecraftforge.fml.network.NetworkEvent
import java.util.function.Supplier

class GrappleHookSyncMessage(private val hookEntityId: Int?) {

    companion object {
        fun read(buf: ByteBuf): GrappleHookSyncMessage {
            return GrappleHookSyncMessage(if (buf.readBoolean()) buf.readInt() else null)
        }
    }

    fun write(buf: ByteBuf) {
        if (hookEntityId == null) {
            buf.writeBoolean(false)
        } else {
            buf.writeBoolean(true)
            buf.writeInt(hookEntityId)
        }
    }

    fun handle(ctxSupplier: Supplier<NetworkEvent.Context>) {
        val ctx = ctxSupplier.get()
        ctx.packetHandled = true
        DistExecutor.safeRunWhenOn(Dist.CLIENT) {
            DistExecutor.SafeRunnable {
                ctx.enqueueWork {
                    val mc = Minecraft.getInstance()
                    mc.player?.get(GrappleHookEntityCap.cap)?.setEntityId(hookEntityId)
                }
            }
        }
    }

}