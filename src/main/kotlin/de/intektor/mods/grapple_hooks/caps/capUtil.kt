package de.intektor.mods.grapple_hooks.caps

import net.minecraft.util.Direction
import net.minecraftforge.common.capabilities.Capability
import net.minecraftforge.common.capabilities.ICapabilityProvider

operator fun <T : Any> ICapabilityProvider.get(cap: Capability<T>, side: Direction? = null): T? {
    // the java annotations are wrong -_-
    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    return getCapability(cap, side).orElse(null)
}