package de.intektor.mods.grapple_hooks.caps

import de.intektor.mods.grapple_hooks.EntityGrappleHook
import de.intektor.mods.grapple_hooks.GrappleHooks
import net.minecraft.client.entity.player.ClientPlayerEntity
import net.minecraft.entity.Entity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.nbt.CompoundNBT
import net.minecraft.nbt.INBT
import net.minecraft.util.Direction
import net.minecraft.util.ResourceLocation
import net.minecraft.world.server.ServerWorld
import net.minecraftforge.common.capabilities.Capability
import net.minecraftforge.common.capabilities.CapabilityInject
import net.minecraftforge.common.capabilities.ICapabilitySerializable
import net.minecraftforge.common.util.LazyOptional
import net.minecraftforge.event.AttachCapabilitiesEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.network.NetworkDirection
import java.lang.ref.WeakReference
import java.util.*

@Mod.EventBusSubscriber(modid = GrappleHooks.ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
internal class GrappleHookEntityCap(
        private val player: PlayerEntity
) : ICapabilitySerializable<CompoundNBT> {

    var grappleEntityData: CompoundNBT? = null
    private var currentGrappleUUID: UUID? = null
    private var entityId: Int? = null
    private var entityWeakRef: WeakReference<EntityGrappleHook>? = null

    private val lazyOptional = LazyOptional.of { this }

    var entity: EntityGrappleHook?
        set(newEntity) {
            if (newEntity == null) {
                clearEntity()
            } else {
                doSetEntity(newEntity)
            }
        }
        get() {
            val referenced = entityWeakRef?.get()
            return if (referenced != null) {
                if (!referenced.isAlive) {
                    clearEntity()
                    null
                } else {
                    referenced
                }
            } else {
                val uuid = currentGrappleUUID
                val entityId = entityId
                val newRef = when {
                    uuid != null -> (player.world as? ServerWorld)?.getEntityByUuid(uuid)
                    entityId != null -> player.world.getEntityByID(entityId)
                    else -> null
                } as? EntityGrappleHook

                if (newRef == null) {
                    clearEntity()
                } else {
                    doSetEntity(newRef)
                }
                newRef
            }
        }

    fun setEntityId(entityId: Int?) {
        clearEntity()
        this.entityId = entityId
    }

    private fun clearEntity() {
        entityWeakRef = null
        currentGrappleUUID = null
        grappleEntityData = null
        entityId = null
        sendSyncMessage(null)
    }

    private fun doSetEntity(entity: EntityGrappleHook) {
        entityWeakRef = WeakReference(entity)
        currentGrappleUUID = entity.uniqueID
        grappleEntityData = null
        entityId = entity.entityId
        sendSyncMessage(entity.entityId)
    }

    private fun sendSyncMessage(id: Int?) {
        (player as? ServerPlayerEntity)?.let { player ->
            GrappleHooks.networkChannel.sendTo(GrappleHookSyncMessage(id), player.connection.netManager, NetworkDirection.PLAY_TO_CLIENT)
        }
    }

    override fun <T : Any?> getCapability(cap: Capability<T>, side: Direction?): LazyOptional<T> {
        return if (cap === Companion.cap) lazyOptional.cast() else LazyOptional.empty()
    }

    override fun serializeNBT(): CompoundNBT {
        return CompoundNBT().also { nbt ->
            entity?.let { grappleEntity ->
                val grappleData = CompoundNBT()
                if (grappleEntity.writeUnlessPassenger(grappleData)) {
                    nbt.put(NBT_GRAPPLE_DATA, grappleData)
                }
            }

        }
    }

    override fun deserializeNBT(nbt: CompoundNBT?) {
        grappleEntityData = nbt?.getCompound(NBT_GRAPPLE_DATA)
    }

    object StorageImpl : Capability.IStorage<GrappleHookEntityCap> {
        override fun writeNBT(capability: Capability<GrappleHookEntityCap>?, instance: GrappleHookEntityCap?, side: Direction?): INBT? {
            throw UnsupportedOperationException("Do not use this capability")
        }

        override fun readNBT(capability: Capability<GrappleHookEntityCap>?, instance: GrappleHookEntityCap?, side: Direction?, nbt: INBT?) {
            throw UnsupportedOperationException("Do not use this capability")
        }
    }

    companion object {

        private const val NBT_GRAPPLE_DATA = "grapple_data"

        @JvmStatic
        @CapabilityInject(GrappleHookEntityCap::class)
        lateinit var cap: Capability<GrappleHookEntityCap>

        val id = ResourceLocation(GrappleHooks.ID, "hook_cap")

        @JvmStatic
        @SubscribeEvent
        fun attachEntityCaps(evt: AttachCapabilitiesEvent<Entity>) {
            val entity = evt.`object`
            if (entity is PlayerEntity) {
                evt.addCapability(id, GrappleHookEntityCap(entity))
            }
        }

        fun forPlayer(player: PlayerEntity): GrappleHookEntityCap {
            return checkNotNull(player[cap]) {
                "GrappleHookEntityCap not attached"
            }
        }


    }

}