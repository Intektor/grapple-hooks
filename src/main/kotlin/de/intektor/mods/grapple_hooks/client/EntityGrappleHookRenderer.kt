package de.intektor.mods.grapple_hooks.client

import com.mojang.blaze3d.matrix.MatrixStack
import com.mojang.blaze3d.vertex.IVertexBuilder
import de.intektor.mods.grapple_hooks.EntityGrappleHook
import de.intektor.mods.grapple_hooks.GrappleHooks
import net.minecraft.client.renderer.IRenderTypeBuffer
import net.minecraft.client.renderer.LightTexture
import net.minecraft.client.renderer.RenderType
import net.minecraft.client.renderer.culling.ClippingHelper
import net.minecraft.client.renderer.entity.EntityRenderer
import net.minecraft.client.renderer.entity.EntityRendererManager
import net.minecraft.client.renderer.entity.MobRenderer
import net.minecraft.client.renderer.texture.OverlayTexture
import net.minecraft.entity.Entity
import net.minecraft.entity.item.HangingEntity
import net.minecraft.util.ResourceLocation
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.MathHelper
import net.minecraft.util.math.vector.Matrix3f
import net.minecraft.util.math.vector.Matrix4f
import net.minecraft.util.math.vector.Vector3f
import net.minecraft.world.LightType
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt


internal class EntityGrappleHookRenderer(p_i46179_1_: EntityRendererManager) : EntityRenderer<EntityGrappleHook>(p_i46179_1_) {

    override fun getEntityTexture(entity: EntityGrappleHook): ResourceLocation {
        return ResourceLocation(GrappleHooks.ID, "textures/entity/grapple_hook.png")
    }

    override fun render(entityIn: EntityGrappleHook, entityYaw: Float, partialTicks: Float, matrixStackIn: MatrixStack, bufferIn: IRenderTypeBuffer, packedLightIn: Int) {
        matrixStackIn.push()
        val scale = 1f
//        matrixStackIn.translate(0.5, 0.5, 0.5)
        matrixStackIn.scale(scale, scale, scale)

        matrixStackIn.rotate(renderManager.cameraOrientation)
        matrixStackIn.rotate(Vector3f.YP.rotationDegrees(180.0f))

        val ivertexbuilder = bufferIn.getBuffer(RenderType.getEntityCutout(getEntityTexture(entityIn)))
        val stack = matrixStackIn.last
        val matrix4f = stack.matrix
        val matrix3f = stack.normal
        drawVertex(matrix4f, matrix3f, ivertexbuilder, -0.5f, -0.25f, 0.0f, 0.0f, 1.0f, 0, 1, 0, packedLightIn)
        drawVertex(matrix4f, matrix3f, ivertexbuilder, 0.5f, -0.25f, 0.0f, 1f, 1.0f, 0, 1, 0, packedLightIn)
        drawVertex(matrix4f, matrix3f, ivertexbuilder, 0.5f, 0.75f, 0.0f, 1.0f, 0.0f, 0, 1, 0, packedLightIn)
        drawVertex(matrix4f, matrix3f, ivertexbuilder, -0.5f, 0.75f, 0.0f, 0.0f, 0.0f, 0, 1, 0, packedLightIn)

        matrixStackIn.pop()

        val leashHolder = entityIn.func_234616_v_()
        if (leashHolder != null) {
            renderLeash(entityIn, partialTicks, matrixStackIn, bufferIn, leashHolder)
        }

        super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn)
    }

    fun drawVertex(
            matrix: Matrix4f?, normals: Matrix3f?, vertexBuilder: IVertexBuilder, offsetX: Float, offsetY: Float, offsetZ: Float, textureX: Float, textureY: Float,
            p_229039_9_: Int, p_229039_10_: Int, p_229039_11_: Int, packedLightIn: Int
    ) {
        vertexBuilder.pos(matrix, offsetX, offsetY, offsetZ).color(255, 255, 255, 255).tex(textureX, textureY)
                .overlay(OverlayTexture.NO_OVERLAY).lightmap(packedLightIn).normal(
                        normals,
                        p_229039_9_.toFloat(), p_229039_11_.toFloat(), p_229039_10_.toFloat()
                ).endVertex()
    }

    override fun shouldRender(livingEntityIn: EntityGrappleHook, camera: ClippingHelper, camX: Double, camY: Double, camZ: Double): Boolean {
        return true
    }

    private fun <E : Entity> renderLeash(entityLivingIn: EntityGrappleHook,
                                         partialTicks: Float,
                                         matrixStackIn: MatrixStack,
                                         bufferIn: IRenderTypeBuffer,
                                         leashHolder: E) {
        matrixStackIn.push()
        val d0 = (MathHelper.lerp(partialTicks * 0.5f, leashHolder.rotationYaw, leashHolder.prevRotationYaw) * (Math.PI.toFloat() / 180f)).toDouble()
        val d1 = (MathHelper.lerp(partialTicks * 0.5f, leashHolder.rotationPitch, leashHolder.prevRotationPitch) * (Math.PI.toFloat() / 180f)).toDouble()
        var d2 = cos(d0)
        var d3 = sin(d0)
        var d4 = sin(d1)
        if (leashHolder is HangingEntity) {
            d2 = 0.0
            d3 = 0.0
            d4 = -1.0
        }

        val d5 = Math.cos(d1)

        val size = leashHolder.getSize(leashHolder.pose)
        val bb = leashHolder.boundingBox

        val d6 = MathHelper.lerp(partialTicks.toDouble(), leashHolder.prevPosX, leashHolder.posX) + bb.xSize / 2.0
        val d7 = MathHelper.lerp(partialTicks.toDouble(), leashHolder.prevPosY, leashHolder.posY) + bb.ySize / 1.2
        val d8 = MathHelper.lerp(partialTicks.toDouble(), leashHolder.prevPosZ, leashHolder.posZ) + bb.zSize / 2.0
        val d9 = Math.PI / 180 + Math.PI / 2.0
        val vector3d = entityLivingIn.func_241205_ce_()
        d2 = Math.cos(d9) * vector3d.z + Math.sin(d9) * vector3d.x
        d3 = Math.sin(d9) * vector3d.z - Math.cos(d9) * vector3d.x
        val d10 = MathHelper.lerp(partialTicks.toDouble(), entityLivingIn.prevPosX, entityLivingIn.posX) + entityLivingIn.width / 2
        val d11 = MathHelper.lerp(partialTicks.toDouble(), entityLivingIn.prevPosY, entityLivingIn.posY) + entityLivingIn.height / 2
        val d12 = MathHelper.lerp(partialTicks.toDouble(), entityLivingIn.prevPosZ, entityLivingIn.posZ) + entityLivingIn.width / 2
//        matrixStackIn.translate(d2, vector3d.y, d3)
        val dX = (d6 - d10).toFloat()
        val dY = (d7 - d11).toFloat()
        val dZ = (d8 - d12).toFloat()

        val ivertexbuilder = bufferIn.getBuffer(RenderType.getLeash())
        val matrix4f = matrixStackIn.last.matrix
        val f4 = 1f / sqrt(dX * dX + dZ * dZ) * 0.025f / 2.0f
        val f5 = dZ * f4
        val f6 = dX * f4
        val blockpos = BlockPos(entityLivingIn.getEyePosition(partialTicks))
        val blockpos1 = BlockPos(leashHolder.getEyePosition(partialTicks))
        val i = getBlockLight(entityLivingIn, blockpos)
        val j = if (leashHolder.isBurning()) 15 else leashHolder.world.getLightFor(LightType.BLOCK, blockpos1)
        val k = entityLivingIn.world.getLightFor(LightType.SKY, blockpos)
        val l = entityLivingIn.world.getLightFor(LightType.SKY, blockpos1)
        renderSide(ivertexbuilder, matrix4f, dX, dY, dZ, i, j, k, l, 0.025f, 0.025f, f5, f6)
        renderSide(ivertexbuilder, matrix4f, dX, dY, dZ, i, j, k, l, 0.025f, 0.0f, f5, f6)

        matrixStackIn.pop()
    }

    private fun renderSide(bufferIn: IVertexBuilder, matrixIn: Matrix4f, p_229119_2_: Float, p_229119_3_: Float, p_229119_4_: Float, blockLight: Int, holderBlockLight: Int, skyLight: Int, holderSkyLight: Int, p_229119_9_: Float, p_229119_10_: Float, p_229119_11_: Float, p_229119_12_: Float) {
        val i = 24
        for (j in 0 until i) {
            val f = j.toFloat() / i
            val k = MathHelper.lerp(f, blockLight.toFloat(), holderBlockLight.toFloat()).toInt()
            val l = MathHelper.lerp(f, skyLight.toFloat(), holderSkyLight.toFloat()).toInt()
            val i1 = LightTexture.packLight(k, l)
            addVertexPair(bufferIn, matrixIn, i1, p_229119_2_, p_229119_3_, p_229119_4_, p_229119_9_, p_229119_10_, i, j, false, p_229119_11_, p_229119_12_)
            addVertexPair(bufferIn, matrixIn, i1, p_229119_2_, p_229119_3_, p_229119_4_, p_229119_9_, p_229119_10_, i, j + 1, true, p_229119_11_, p_229119_12_)
        }
    }

    fun addVertexPair(bufferIn: IVertexBuilder, matrixIn: Matrix4f, packedLight: Int, p_229120_3_: Float, p_229120_4_: Float, p_229120_5_: Float, p_229120_6_: Float, p_229120_7_: Float, p_229120_8_: Int, p_229120_9_: Int, p_229120_10_: Boolean, p_229120_11_: Float, p_229120_12_: Float) {
        var f = 0.5f
        var f1 = 0.4f
        var f2 = 0.1f
        if (p_229120_9_ % 2 == 0) {
            f *= 0.7f
            f1 *= 0.7f
            f2 *= 0.7f
        }
        val f3 = p_229120_9_.toFloat() / p_229120_8_.toFloat()
        val renderX = p_229120_3_ * f3
        val renderY = p_229120_4_ - p_229120_4_ * (1.0f - f3)
        val renderZ = p_229120_5_ * f3
        if (!p_229120_10_) {
            bufferIn.pos(matrixIn, renderX + p_229120_11_, renderY + p_229120_6_ - p_229120_7_, renderZ - p_229120_12_).color(f, f1, f2, 1.0f).lightmap(packedLight).endVertex()
        }
        bufferIn.pos(matrixIn, renderX - p_229120_11_, renderY + p_229120_7_, renderZ + p_229120_12_).color(f, f1, f2, 1.0f).lightmap(packedLight).endVertex()
        if (p_229120_10_) {
            bufferIn.pos(matrixIn, renderX + p_229120_11_, renderY + p_229120_6_ - p_229120_7_, renderZ - p_229120_12_).color(f, f1, f2, 1.0f).lightmap(packedLight).endVertex()
        }
    }
}