package de.intektor.mods.grapple_hooks.client

import de.intektor.mods.grapple_hooks.*
import net.minecraft.client.Minecraft
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.MoverType
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.util.math.vector.Vector3d
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.event.TickEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.client.registry.RenderingRegistry
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent
import kotlin.math.*

@Mod.EventBusSubscriber(Dist.CLIENT, modid = GrappleHooks.ID, bus = Mod.EventBusSubscriber.Bus.MOD)
object ClientEventHandler {

    @JvmStatic
    @SubscribeEvent
    fun register(event: FMLClientSetupEvent) {
        RenderingRegistry.registerEntityRenderingHandler(GrappleHooks.entityGrapplingHook.get()) {
            EntityGrappleHookRenderer(it)
        }

        MinecraftForge.EVENT_BUS.register(this)
    }
}