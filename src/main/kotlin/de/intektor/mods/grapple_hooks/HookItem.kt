package de.intektor.mods.grapple_hooks

import de.intektor.mods.grapple_hooks.caps.GrappleHookEntityCap
import net.minecraft.enchantment.EnchantmentHelper
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.math.vector.Vector3d
import net.minecraft.world.World
import kotlin.math.cos
import kotlin.math.sin

internal class HookItem(properties: Properties, val tier: HookTier) : Item(properties) {

    override fun onItemRightClick(world: World, player: PlayerEntity, hand: Hand): ActionResult<ItemStack> {
        val heldItem = player.getHeldItem(hand)
        if (!world.isRemote && player is ServerPlayerEntity) {
            val cap = GrappleHookEntityCap.forPlayer(player)
            val currentHook = cap.entity

            val spawnNewHook = if (currentHook != null) {
                if (currentHook.isDragging) {
                    if (currentHook.isHookStopped) {
                        currentHook.remove()
                        cap.entity = null
                        true
                    } else {
                        currentHook.stopHook()
                        false
                    }
                } else if (currentHook.isReturning) {
                    currentHook.remove()
                    cap.entity = null
                    true
                } else {
                    currentHook.isReturning = true
                    false
                }
            } else {
                true
            }

            if (spawnNewHook) {
                val pullSpeedLevel = EnchantmentHelper.getEnchantmentLevel(GrappleHooks.pullSpeedEnchantment.get(), heldItem)
                val additionalRangeLevel = EnchantmentHelper.getEnchantmentLevel(GrappleHooks.ropeDistanceEnchantment.get(), heldItem)

                val newHook = EntityGrappleHook(world, tier, pullSpeedLevel, additionalRangeLevel)

                val pitch = Math.toRadians(player.rotationPitch.toDouble())
                val yaw = Math.toRadians(player.rotationYaw.toDouble())

                val motionX = -sin(yaw) * cos(pitch)
                val motionY = -sin(pitch)
                val motionZ = cos(yaw) * cos(pitch)

                newHook.motion = (Vector3d(motionX, motionY, motionZ) * 10.0) + player.motion

                newHook.setPosition(player.posX, player.posY + player.eyeHeight, player.posZ)
                newHook.setShooter(player)
                if (world.addEntity(newHook)) {
                    cap.entity = newHook
                    heldItem.damageItem(1, player) {
                        player.sendBreakAnimation(hand)
                    }
                    return ActionResult.resultConsume(heldItem)
                }
            }
        }
        return ActionResult.resultSuccess(heldItem)
    }

    override fun getIsRepairable(toRepair: ItemStack, repair: ItemStack): Boolean {
        return (tier.repairMaterial ?: return false).test(repair)
    }

    override fun getItemEnchantability(stack: ItemStack?): Int = tier.enchantability

}