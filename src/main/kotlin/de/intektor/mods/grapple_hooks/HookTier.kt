package de.intektor.mods.grapple_hooks

import com.google.common.collect.ImmutableMap
import net.minecraft.item.crafting.Ingredient
import net.minecraft.tags.ItemTags
import net.minecraftforge.common.Tags
import net.minecraftforge.fml.RegistryObject

internal enum class HookTier(
    val id: String,
    val maxDamage: Int,
    val range: Float,
    val enchantability: Int,
    repairMaterial: (() -> Ingredient)? = null
) {

    WOOD(
        "wooden",
        30,
        5f,
        15,
        { Ingredient.fromTag(ItemTags.PLANKS) }
    ),
    STONE(
        "stone",
        50,
        10f,
        5,
        { Ingredient.fromTag(ItemTags.field_232909_aa_) }
    ),
    IRON(
        "iron",
        200,
        20f,
        14,
        { Ingredient.fromTag(Tags.Items.INGOTS_IRON) }
    ),
    GOLD(
        "golden",
        250,
        40f,
        22,
        { Ingredient.fromTag(Tags.Items.INGOTS_GOLD) }
    ),
    DIAMOND(
        "diamond",
        1000,
        50f,
        10,
        { Ingredient.fromTag(Tags.Items.GEMS_DIAMOND) }
    );

    val repairMaterial: Ingredient? by repairMaterial?.let { lazy(it) } ?: lazyOf(null)

    fun item(): RegistryObject<HookItem> {
        return checkNotNull(GrappleHooksItems.hooks[this])
    }

    fun getDragSpeedModifier(pullSpeedLevel: Int): Double = 0.5 * pullSpeedLevel

    companion object {

        private val nameMap = ImmutableMap.copyOf(values().associateBy { it.name })

        fun fromName(name: String): HookTier? {
            return nameMap[name]
        }

    }

}
