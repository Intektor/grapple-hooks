package de.intektor.mods.grapple_hooks

import net.minecraft.inventory.IInventory
import net.minecraft.item.ItemStack

internal fun IInventory.seq(): Sequence<ItemStack> {
    return sequence {
        for (i in 0 until sizeInventory) {
            yield(getStackInSlot(i))
        }
    }
}