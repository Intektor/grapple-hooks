package de.intektor.mods.grapple_hooks

import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemStack
import net.minecraft.util.NonNullList
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries

internal object GrappleHooksItems {

    val group = object : ItemGroup(GrappleHooks.ID) {

        init {
            setRelevantEnchantmentTypes(GrappleHooks.hookEnchantmentType)
        }

        private val sorter: Comparator<ItemStack> = compareBy { stack ->
            val item = stack.item
            if (item is HookItem) {
                item.tier.ordinal
            } else {
                Int.MAX_VALUE
            }
        }

        override fun createIcon(): ItemStack = ItemStack(HookTier.DIAMOND.item().get())
        override fun fill(items: NonNullList<ItemStack>) {
            super.fill(items)
            items.sortWith(sorter)
        }
    }

    val items: DeferredRegister<Item> = DeferredRegister.create(ForgeRegistries.ITEMS, GrappleHooks.ID)
    val hooks = HookTier.values().associate { tier ->
        tier to items.register("hook_${tier.id}") {
            HookItem(
                Item.Properties().also {
                    it.maxDamage(tier.maxDamage)
                    it.group(group)
                },
                tier
            )
        }
    }
}