package de.intektor.mods.grapple_hooks

import net.minecraft.entity.*
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.ServerPlayerEntity
import net.minecraft.entity.projectile.ThrowableEntity
import net.minecraft.nbt.CompoundNBT
import net.minecraft.network.IPacket
import net.minecraft.network.PacketBuffer
import net.minecraft.network.datasync.DataSerializers
import net.minecraft.network.datasync.EntityDataManager
import net.minecraft.util.math.*
import net.minecraft.util.math.vector.Vector3d
import net.minecraft.world.World
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData
import net.minecraftforge.fml.network.NetworkHooks
import kotlin.experimental.and
import kotlin.experimental.inv
import kotlin.experimental.or
import kotlin.math.*

internal class EntityGrappleHook(
        type: EntityType<out EntityGrappleHook>,
        world: World,
        var tier: HookTier?,
        var pullSpeedLevel: Int = 0,
        var additionalRangeLevel: Int = 0
) : ThrowableEntity(type, world), IEntityAdditionalSpawnData {

    constructor(world: World, tier: HookTier?, pullSpeedLevel: Int = 0, additionalRangeLevel: Int = 0) :
            this(GrappleHooks.entityGrapplingHook.get(), world, tier, pullSpeedLevel, additionalRangeLevel)

    // for client
    constructor(type: EntityType<out EntityGrappleHook>, world: World) : this(type, world, null)

    init {
        setNoGravity(true)
    }

    internal companion object {

        private const val FLAG_IS_DRAGGING: Byte = 0b01
        private const val FLAG_IS_RETURNING: Byte = 0b10
        private const val FLAG_HAS_STOPPED_PLAYER: Byte = 0b100

        private val FLAGS = EntityDataManager.createKey(EntityGrappleHook::class.java, DataSerializers.BYTE)
        private val STOPPED_HOOK_LENGTH = EntityDataManager.createKey(EntityGrappleHook::class.java, DataSerializers.FLOAT)
    }

    private var flags: Byte
        get() = dataManager[FLAGS] ?: 0
        set(value) = dataManager.set(FLAGS, value)

    private var attachedBlock: BlockPos? = null

    /**
     * If this entity hit a block and should drag the shooter towards it now
     */
    var isDragging: Boolean
        get() = flags and FLAG_IS_DRAGGING != 0.toByte()
        set(value) {
            flags = if (value) flags or FLAG_IS_DRAGGING else flags and FLAG_IS_DRAGGING.inv()
        }

    var isReturning: Boolean
        get() = flags and FLAG_IS_RETURNING != 0.toByte()
        set(value) {
            flags = if (value) flags or FLAG_IS_RETURNING else flags and FLAG_IS_RETURNING.inv()
        }

    var stoppedPlayer: Boolean
        get() = flags and FLAG_HAS_STOPPED_PLAYER != 0.toByte()
        set(value) {
            flags = if (value) flags or FLAG_HAS_STOPPED_PLAYER else flags and FLAG_HAS_STOPPED_PLAYER.inv()
        }

    var stoppedHookLength: Float
        get() = dataManager[STOPPED_HOOK_LENGTH]
        private set(value) = dataManager.set(STOPPED_HOOK_LENGTH, value)

    val isHookStopped: Boolean
        get() = stoppedHookLength != -1f

    /**
     * The motion the shooter had last tick, used to calculate damage when flying against wall
     */
    var prevShooterMotion: Vector3d = Vector3d.ZERO

    override fun createSpawnPacket(): IPacket<*> = NetworkHooks.getEntitySpawningPacket(this)

    override fun registerData() {
        dataManager.register(FLAGS, 0)
        dataManager.register(STOPPED_HOOK_LENGTH, -1f)
    }

    fun stopHook() {
        if (isDragging && !isHookStopped) {
            val shooter = func_234616_v_()
            if (shooter == null) {
                if (!world.isRemote) setDead()
            } else {
                stoppedHookLength = sqrt(getDistanceToShooter(shooter)).toFloat()
            }
        }
    }

    override fun tick() {
        super.tick()

        val shooter = func_234616_v_()
        if (shooter == null || shooter.isSneaking) {
            if (!world.isRemote) setDead()
        } else {
            if (shooter is ServerPlayerEntity) {
                shooter.connection.floatingTickCount = 0
            }

            val dX = shooter.posX - posX
            val dY = (shooter.eyeHeight + shooter.posY) - posY
            val dZ = shooter.posZ - posZ
            val distance = dX * dX + dY * dY + dZ * dZ
            val tier = tier
            if (!world.isRemote && tier != null) {
                val hookRange = tier.range + (additionalRangeLevel * 10f)
                if (distance > hookRange * hookRange) {
                    isReturning = true
                    motion = motion.scale(-1.0)
                }

                if (distance < 4 && isReturning && !isDragging) {
                    setDead()
                }
            }

            if (shooter.world != this.world) {
                setDead()
            }

            val dragVector = Vector3d(posX - shooter.posX, posY - (shooter.posY + shooter.eyeHeight), posZ - shooter.posZ)
                    .normalize()
                    .scale(3.0)

            if (isDragging) {
                if (!world.isRemote) {
                    val attachedBlock = this.attachedBlock
                    if (attachedBlock == null || world.isAirBlock(attachedBlock)) {
                        remove()
                    }
                }

                if (distance < 5) {
                    stopHook()
                }

                if (!world.isRemote) {
                    if (!GrappleHooks.config.useRealisticGrappling) {
                        //For non realistic grappling we just set the player damage to always 0
                        shooter.fallDistance = 0f
                    }
                }
            } else if (isReturning) {
                motion = dragVector.scale(-1.0)
            }
        }
    }

    internal fun getGoalOffset(currentPosition: Vector3d): Vector3d {
        val currentOffset = currentPosition.subtract(positionVec)
        val goalVector = currentOffset.normalize().scale(stoppedHookLength.toDouble())
        return goalVector.subtract(currentOffset)
    }

    fun getDistanceToShooter(shooter: Entity): Double {
        val dX = shooter.posX - posX
        val dY = (shooter.eyeHeight + shooter.posY) - posY
        val dZ = shooter.posZ - posZ

        return dX * dX + dY * dY + dZ * dZ
    }

    override fun onImpact(rayTraceResult: RayTraceResult) {
        super.onImpact(rayTraceResult)

        if (rayTraceResult is BlockRayTraceResult) {
            if (!world.isRemote) {
                isDragging = true
                isReturning = false
                attachedBlock = rayTraceResult.pos
            }

            setMotion(0.0, 0.0, 0.0)
            setPosition(rayTraceResult.hitVec.x, rayTraceResult.hitVec.y, rayTraceResult.hitVec.z)
        } else if (rayTraceResult is EntityRayTraceResult) {
            if (!world.isRemote) {
                isReturning = true
            }

            val entity = rayTraceResult.entity
            val shooter = func_234616_v_()
            if (GrappleHooks.config.isEntityHookingAllowed && entity != shooter && (entity !is PlayerEntity || GrappleHooks.config.isPlayerHookingAllowed)) {
                entity.startRiding(this, true)
            }
        }
    }

    override fun getBoundingBox(): AxisAlignedBB {
        return AxisAlignedBB(posX - 0.5, posY - 0.5, posZ - 0.5, posX + 0.5, posY + 0.5, posZ + 0.5)
    }

    override fun writeAdditional(compound: CompoundNBT) {
        super.writeAdditional(compound)
        val tier = tier
        if (tier != null) {
            compound.putString("tier", tier.name)
        }
        compound.putInt("pullSpeedLevel", this.pullSpeedLevel)
        compound.putInt("additionalRangeLevel", this.additionalRangeLevel)
        compound.putByte("hookFlags", this.flags)
        compound.putFloat("stoppedHookLength", this.stoppedHookLength)
        val pos = attachedBlock
        if (pos != null) {
            compound.putIntArray("attachedBlock", intArrayOf(pos.x, pos.y, pos.z))
        }
    }

    override fun readAdditional(compound: CompoundNBT) {
        super.readAdditional(compound)
        this.tier = compound.getString("tier")
                .takeUnless { it.isBlank() }
                ?.let { HookTier.fromName(it) }

        this.pullSpeedLevel = compound.getInt("pullSpeedLevel")
        this.additionalRangeLevel = compound.getInt("additionalRangeLevel")
        this.flags = compound.getByte("hookFlags")
        this.stoppedHookLength = compound.getFloat("stoppedHookLength")
        val attachedBlock = compound.getIntArray("attachedBlock")
        this.attachedBlock = if (attachedBlock.size == 3) {
            BlockPos(attachedBlock[0], attachedBlock[1], attachedBlock[2])
        } else {
            null
        }
    }

    override fun writeSpawnData(buffer: PacketBuffer) {
        buffer.writeInt(func_234616_v_()?.entityId ?: 0)
        buffer.writeVarInt(pullSpeedLevel)
        buffer.writeByte(tier?.ordinal ?: -1)
        buffer.writeInt(additionalRangeLevel)

        val motion = this.motion
        buffer.writeFloat(motion.x.toFloat())
        buffer.writeFloat(motion.y.toFloat())
        buffer.writeFloat(motion.z.toFloat())
    }

    override fun readSpawnData(buffer: PacketBuffer) {
        setShooter(world.getEntityByID(buffer.readInt()))
        pullSpeedLevel = buffer.readVarInt()
        tier = HookTier.values().getOrNull(buffer.readByte().toInt())
        additionalRangeLevel = buffer.readInt()

        val velX = buffer.readFloat().toDouble()
        val velY = buffer.readFloat().toDouble()
        val velZ = buffer.readFloat().toDouble()
        setVelocity(velX, velY, velZ)
    }
}