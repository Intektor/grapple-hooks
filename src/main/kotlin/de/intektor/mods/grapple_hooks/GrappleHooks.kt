package de.intektor.mods.grapple_hooks

import de.intektor.mods.grapple_hooks.caps.GrappleHookEntityCap
import de.intektor.mods.grapple_hooks.config.GrappleHooksConfig
import de.intektor.mods.grapple_hooks.ench.HookEnchantment
import de.intektor.mods.grapple_hooks.recipe.HookPreservingRecipe
import net.minecraft.enchantment.EnchantmentType
import net.minecraft.entity.EntityClassification
import net.minecraft.entity.EntityType
import net.minecraftforge.common.ForgeConfigSpec
import net.minecraftforge.common.capabilities.CapabilityManager
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.ModLoadingContext
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.config.ModConfig
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext
import net.minecraftforge.fml.network.simple.SimpleChannel
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries
import kotlin.properties.Delegates

@Mod(GrappleHooks.ID)
@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
internal class GrappleHooks {

    init {
        for (reg in arrayOf(GrappleHooksItems.items, entityRegistry, recipeSerializers, enchantments)) {
            reg.register(FMLJavaModLoadingContext.get().modEventBus)
        }

        val config = ForgeConfigSpec.Builder().apply {
            val isEntityHookingAllowedSpec = comment("Should hooking entities be possible?")
                    .define("allowEntityHooking", true)

            val isPlayerHookingAllowedSpec = comment("Do you want players to be able to be hooked")
                    .define("allowPlayerHooking", true)

            val useRealisticGrapplingSpec =
                    comment("""Should grapples use realistic dragging and such also apply realistic fall damage when being
                        hooked downwards?
                    """)
                            .define("realisticGrappling", true)

            val applySwingDamageSpec = comment("If realistic grappling is enabled, should players getting thrown against walls take damage?")
                    .define("realisticGrapplingWallDamage", false)

            config = GrappleHooksConfig(isEntityHookingAllowedSpec, isPlayerHookingAllowedSpec, useRealisticGrapplingSpec, applySwingDamageSpec)
        }.build()

        ModLoadingContext.get().activeContainer.apply {
            addConfig(ModConfig(ModConfig.Type.SERVER, config, this))
        }

        networkChannel = createNetworkChannel()
    }

    companion object {
        const val ID = "grapple_hooks"

        var networkChannel by Delegates.notNull<SimpleChannel>()

        @JvmStatic
        @SubscribeEvent
        fun commonInit(evt: FMLCommonSetupEvent) {
            CapabilityManager.INSTANCE.register(GrappleHookEntityCap::class.java, GrappleHookEntityCap.StorageImpl) {
                throw UnsupportedOperationException("Do not use this capability")
            }
        }

        val hookEnchantmentType = EnchantmentType.create("${ID}_hook") { it is HookItem }

        private val entityRegistry = DeferredRegister.create(ForgeRegistries.ENTITIES, ID)

        val entityGrapplingHook = entityRegistry.register("grappling_hook") {
            EntityType.Builder.create(::EntityGrappleHook, EntityClassification.MISC)
                .setUpdateInterval(20)
                .setShouldReceiveVelocityUpdates(false)
                .size(0.5f, 0.5f)
                .build("grappling_hook")
        }

        val recipeSerializers = DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, ID)
        val hookPreservingRecipe = recipeSerializers.register("hook_preserving", HookPreservingRecipe::Serializer)

        val enchantments = DeferredRegister.create(ForgeRegistries.ENCHANTMENTS, ID)
        val pullSpeedEnchantment = enchantments.register("pull_speed") { HookEnchantment() }
        val ropeDistanceEnchantment = enchantments.register("rope_distance") { HookEnchantment() }

        lateinit var config: GrappleHooksConfig
    }

}