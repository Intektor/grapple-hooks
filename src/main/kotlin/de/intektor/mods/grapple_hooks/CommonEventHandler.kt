package de.intektor.mods.grapple_hooks

import de.intektor.mods.grapple_hooks.caps.GrappleHookEntityCap
import de.intektor.mods.grapple_hooks.caps.get
import net.minecraft.entity.EntityType
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.MoverType
import net.minecraft.util.DamageSource
import net.minecraft.util.math.vector.Vector3d
import net.minecraftforge.event.TickEvent
import net.minecraftforge.event.entity.player.PlayerEvent
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import kotlin.math.*

@Mod.EventBusSubscriber(modid = GrappleHooks.ID, bus = Mod.EventBusSubscriber.Bus.FORGE)
object CommonEventHandler {

    @JvmStatic
    @SubscribeEvent
    fun playerTick(event: TickEvent.PlayerTickEvent) {
        val shooter = event.player
        val hook = shooter[GrappleHookEntityCap.cap]?.entity
        if (hook != null && hook.isAlive) {
            if (event.phase == TickEvent.Phase.START) {
                val dragStrength =
                        (0.8 + (hook.tier?.getDragSpeedModifier(hook.pullSpeedLevel) ?: 0.0)) * (
                                if (GrappleHooks.config.useRealisticGrappling) 0.4 else 1.0
                                )

                var dragVector = Vector3d(
                        hook.posX - shooter.posX, hook.posY - (shooter.posY + shooter.eyeHeight),
                        hook.posZ - shooter.posZ
                )
                        .normalize()
                        .scale(dragStrength)

                val dX = shooter.posX - hook.posX
                val dY = (shooter.eyeHeight + shooter.posY) - hook.posY
                val dZ = shooter.posZ - hook.posZ
                val distance = dX * dX + dY * dY + dZ * dZ

                if (hook.isDragging) {
                    val prevStoppedPlayer = hook.stoppedPlayer

                    if (hook.isHookStopped) {
                        if (!hook.stoppedPlayer) {
                            hook.stoppedPlayer = true
                        } else if (shooter is LivingEntity) {
                            val distanceNormal = sqrt(distance)
                            val angle = asin(abs(dY) / distanceNormal)
                            dragVector = Vector3d.ZERO

                            val ropeLengthOffset = distanceNormal - hook.stoppedHookLength.toDouble()

                            val shooterEyePos = shooter.positionVec.add(0.0, shooter.eyeHeight.toDouble(), 0.0)

                            val forceAmount = hook.getGoalOffset(shooterEyePos)

                            if (ropeLengthOffset > 0.0) {
                                if (forceAmount.length() >= 0.01) {
                                    shooter.move(MoverType.SELF, forceAmount)
                                    shooter.fallDistance = 0.0f
                                }

                                val forceDown = abs(shooter.motion.y)
                                val angleTowards = atan2(-dZ, dX)
                                dragVector = dragVector.add(
                                        Vector3d(
                                                -cos(angleTowards) * cos(angle) * forceDown,
                                                forceAmount.y,
                                                sin(angleTowards) * cos(angle) * forceDown
                                        )
                                )
                            } else {
                                dragVector = dragVector.mul(0.0, 0.0, 0.0)
                            }
                        }
                    } else if (distance < 5) {
                        val prop = (distance / 5)
                        dragVector = dragVector.scale(prop)
                    }

                    if (!prevStoppedPlayer && !GrappleHooks.config.useRealisticGrappling) {
                        shooter.motion = dragVector
                    } else {
                        shooter.motion = shooter.motion.scale(1 / 0.98).add(dragVector)
                    }
                }
            } else if (event.phase == TickEvent.Phase.END) {
                //Check that the player is really swinging here
                if (hook.isHookStopped && !hook.isReturning && GrappleHooks.config.useRealisticGrappling && GrappleHooks.config.applySwingDamage) {
                    if (shooter.collidedHorizontally && !shooter.world.isRemote) {
                        val horizontalMotionAmount = LivingEntity.horizontalMag(hook.prevShooterMotion)
                        val verticalAngle = atan2(shooter.motion.y, sqrt(horizontalMotionAmount))
                        val approxDamage = horizontalMotionAmount * sin(verticalAngle) * 10.0
                        if (approxDamage >= 1) {
                            shooter.attackEntityFrom(DamageSource.FLY_INTO_WALL, approxDamage.toFloat())
                        }
                    }
                    hook.prevShooterMotion = shooter.motion
                }
            }
        }
    }


    @JvmStatic
    @SubscribeEvent
    fun playerLogin(event: PlayerEvent.PlayerLoggedInEvent) {
        if (!event.player.world.isRemote) {
            val cap = GrappleHookEntityCap.forPlayer(event.player)
            val hookData = cap.grappleEntityData
            if (hookData != null) {
                cap.grappleEntityData = null
                cap.entity = EntityType.loadEntityAndExecute(hookData, event.player.world) { newEntity ->
                    newEntity.also { event.player.world.addEntity(it) }
                } as? EntityGrappleHook
            }
        }
    }

}