package de.intektor.mods.grapple_hooks

import net.minecraft.item.Item
import net.minecraft.item.ItemStack

internal fun ItemStack.copyWith(item: Item = this.item): ItemStack {
    return ItemStack(item, this.count).also {
        it.animationsToGo = this.animationsToGo
        it.tag = tag?.copy()
    }
}