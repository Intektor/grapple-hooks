package de.intektor.mods.grapple_hooks

import de.intektor.mods.grapple_hooks.caps.GrappleHookSyncMessage
import net.minecraft.util.ResourceLocation
import net.minecraftforge.fml.network.NetworkDirection
import net.minecraftforge.fml.network.NetworkRegistry
import net.minecraftforge.fml.network.simple.SimpleChannel
import java.util.*

val networkChannelId = ResourceLocation(GrappleHooks.ID, "network")
private val networkVersion = "1"

fun createNetworkChannel(): SimpleChannel {
    return NetworkRegistry.newSimpleChannel(networkChannelId, { networkVersion }, networkVersion::equals, networkVersion::equals).apply {
        registerMessage(
            0,
            GrappleHookSyncMessage::class.java,
            GrappleHookSyncMessage::write,
            GrappleHookSyncMessage.Companion::read,
            GrappleHookSyncMessage::handle,
            Optional.of(NetworkDirection.PLAY_TO_CLIENT)
        )
    }
}