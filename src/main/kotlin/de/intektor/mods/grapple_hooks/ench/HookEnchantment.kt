package de.intektor.mods.grapple_hooks.ench

import de.intektor.mods.grapple_hooks.GrappleHooks
import net.minecraft.enchantment.Enchantment
import net.minecraft.inventory.EquipmentSlotType

class HookEnchantment : Enchantment(
    Rarity.UNCOMMON,
    GrappleHooks.hookEnchantmentType,
    arrayOf(EquipmentSlotType.MAINHAND, EquipmentSlotType.OFFHAND)
) {

    override fun getMaxLevel(): Int {
        return 3
    }

}