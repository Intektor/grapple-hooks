package de.intektor.mods.grapple_hooks.config

import net.minecraftforge.common.ForgeConfigSpec

/**
 * Config holder for grapple hooks
 */
internal class GrappleHooksConfig(
        private val isEntityHookingAllowedSpec: ForgeConfigSpec.BooleanValue,
        private val isPlayerHookingAllowedSpec: ForgeConfigSpec.BooleanValue,
        private val useRealisticGrapplingSpec: ForgeConfigSpec.BooleanValue,
        private val applySwingDamageSpec: ForgeConfigSpec.BooleanValue
) {

    val isEntityHookingAllowed: Boolean get() = isEntityHookingAllowedSpec.get()
    val isPlayerHookingAllowed: Boolean get() = isPlayerHookingAllowedSpec.get()
    val useRealisticGrappling: Boolean get() = useRealisticGrapplingSpec.get()
    val applySwingDamage: Boolean get() = applySwingDamageSpec.get()

}