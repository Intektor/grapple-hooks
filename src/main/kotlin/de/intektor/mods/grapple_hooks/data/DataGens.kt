package de.intektor.mods.grapple_hooks.data

import de.intektor.mods.grapple_hooks.GrappleHooks
import de.intektor.mods.grapple_hooks.GrappleHooksItems
import de.intektor.mods.grapple_hooks.HookTier
import de.intektor.mods.grapple_hooks.recipe.HookPreservingRecipe
import net.minecraft.advancements.criterion.InventoryChangeTrigger
import net.minecraft.advancements.criterion.ItemPredicate
import net.minecraft.data.DataGenerator
import net.minecraft.data.IFinishedRecipe
import net.minecraft.data.RecipeProvider
import net.minecraft.data.ShapedRecipeBuilder
import net.minecraft.item.Items
import net.minecraft.tags.ItemTags
import net.minecraft.util.ResourceLocation
import net.minecraftforge.client.model.generators.ItemModelProvider
import net.minecraftforge.client.model.generators.ModelBuilder
import net.minecraftforge.common.data.ExistingFileHelper
import net.minecraftforge.common.data.LanguageProvider
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent
import java.util.function.Consumer

@Mod.EventBusSubscriber(modid = GrappleHooks.ID, bus = Mod.EventBusSubscriber.Bus.MOD)
internal object DataGens {

    @JvmStatic
    @SubscribeEvent
    fun gather(evt: GatherDataEvent) {
        arrayOf(
                ItemModels(evt.generator, evt.existingFileHelper),
                Lang(evt.generator, "en_us"),
                Recipes(evt.generator)
        ).forEach { evt.generator.addProvider(it) }
    }

    private class Lang(gen: DataGenerator, locale: String) : LanguageProvider(gen, GrappleHooks.ID, locale) {
        override fun addTranslations() {
            add("itemGroup.${GrappleHooksItems.group.path}", "Grapple Hooks")
            addItem(HookTier.WOOD.item(), "Wooden Hook")
            addItem(HookTier.STONE.item(), "Stone Hook")
            addItem(HookTier.IRON.item(), "Iron Hook")
            addItem(HookTier.GOLD.item(), "Golden Hook")
            addItem(HookTier.DIAMOND.item(), "Diamond Hook")
            addEnchantment(GrappleHooks.pullSpeedEnchantment, "Hook Speed")
            addEnchantment(GrappleHooks.ropeDistanceEnchantment, "Rope Length")
        }
    }

    private class ItemModels(
            generator: DataGenerator, existingFileHelper: ExistingFileHelper
    ) : ItemModelProvider(generator, GrappleHooks.ID, existingFileHelper) {

        override fun registerModels() {
            for (tier in HookTier.values()) {
                withExistingParent(tier.item().id.path, ResourceLocation("minecraft", "item/generated"))
                        .texture("layer0", ResourceLocation(GrappleHooks.ID, "item/${tier.id}_grapple"))
                        // @formatter:off
                    .transforms()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_RIGHT)
                            .rotation(0f, 75f, -37f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(0.78f)
                        .end()
                        .transform(ModelBuilder.Perspective.FIRSTPERSON_LEFT)
                            .rotation(0f, -75f, 37f)
                            .translation(1.13f, 3.2f, 1.13f)
                            .scale(0.78f)
                        .end()
                    .end()
                    // @formatter:on
            }
        }
    }

    private class Recipes(generatorIn: DataGenerator) : RecipeProvider(generatorIn) {

        override fun registerRecipes(c: Consumer<IFinishedRecipe>) {
            ShapedRecipeBuilder.shapedRecipe(HookTier.WOOD.item().get())
                    .addCriterion(
                            "has_arrows",
                            InventoryChangeTrigger.Instance.forItems(
                                    ItemPredicate.Builder.create().tag(ItemTags.ARROWS).build()
                            )
                    )
                    .addCriterion(
                            "has_dispenser",
                            InventoryChangeTrigger.Instance.forItems(
                                    ItemPredicate.Builder.create().item(Items.DISPENSER).build()
                            )
                    )
                    .patternLine("#/#")
                    .patternLine("#*#")
                    .patternLine("###")
                    .key('#', ItemTags.PLANKS)
                    .key('/', ItemTags.ARROWS)
                    .key('*', Items.DISPENSER)
                    .build(c, HookTier.WOOD.item().id)

            for ((tier, nextTier) in HookTier.values().toList().zipWithNext()) {
                val repairMaterial = nextTier.repairMaterial
                if (repairMaterial != null) {
                    lateinit var wrapped: IFinishedRecipe
                    ShapedRecipeBuilder.shapedRecipe(nextTier.item().get())
                            .addCriterion(
                                    "has_previous",
                                    InventoryChangeTrigger.Instance.forItems(tier.item().get())
                            )
                            .patternLine("###")
                            .patternLine("#/#")
                            .patternLine("###")
                            .key('#', repairMaterial)
                            .key('/', tier.item().get())
                            .build { r -> wrapped = r }

                    c.accept(
                            HookPreservingRecipe.DataGenRepr(wrapped)
                    )
                }
            }
        }
    }

}