package de.intektor.mods.grapple_hooks.recipe

import com.google.gson.JsonObject
import com.google.gson.JsonSyntaxException
import de.intektor.mods.grapple_hooks.GrappleHooks
import de.intektor.mods.grapple_hooks.HookItem
import de.intektor.mods.grapple_hooks.seq
import net.minecraft.data.IFinishedRecipe
import net.minecraft.inventory.CraftingInventory
import net.minecraft.item.ItemStack
import net.minecraft.item.crafting.ICraftingRecipe
import net.minecraft.item.crafting.IRecipe
import net.minecraft.item.crafting.IRecipeSerializer
import net.minecraft.item.crafting.RecipeManager
import net.minecraft.network.PacketBuffer
import net.minecraft.util.JSONUtils
import net.minecraft.util.ResourceLocation
import net.minecraft.world.World
import net.minecraftforge.registries.ForgeRegistries
import net.minecraftforge.registries.ForgeRegistryEntry

internal class HookPreservingRecipe(
    private val delegate: ICraftingRecipe
) : ICraftingRecipe by delegate {

    override fun matches(inv: CraftingInventory, worldIn: World): Boolean {
        val result = delegate.matches(inv, worldIn)
        return result
    }

    override fun getCraftingResult(inv: CraftingInventory): ItemStack {
        val result = delegate.getCraftingResult(inv)
        val resultItem = result.item
        if (resultItem is HookItem) {
            val inputHook = inv.seq().singleOrNull { it.item is HookItem }
            if (inputHook != null) {
                inputHook.tag?.copy()?.let { inputTag ->
                    result.tag = result.tag?.let { inputTag.merge(it) } ?: inputTag
                }
                result.damage = inputHook.damage
            }
        }
        return result
    }

    override fun getSerializer(): IRecipeSerializer<*> {
        return GrappleHooks.hookPreservingRecipe.get()
    }

    class DataGenRepr(private val wrapped: IFinishedRecipe) : IFinishedRecipe by wrapped {
        override fun serialize(json: JsonObject) {
            json.add("wrapped", wrapped.recipeJson)
        }

        override fun getSerializer(): IRecipeSerializer<*> {
            return GrappleHooks.hookPreservingRecipe.get()
        }
    }

    class Serializer : ForgeRegistryEntry<IRecipeSerializer<*>>(), IRecipeSerializer<HookPreservingRecipe> {

        override fun read(recipeId: ResourceLocation, json: JsonObject): HookPreservingRecipe {
            val wrapped = JSONUtils.getJsonObject(json, "wrapped")
            return HookPreservingRecipe(
                RecipeManager.deserializeRecipe(recipeId, wrapped).requireCrafting()
            )
        }

        override fun read(recipeId: ResourceLocation, buffer: PacketBuffer): HookPreservingRecipe? {
            val wrappedSerializer = buffer.readRegistryIdUnsafe(ForgeRegistries.RECIPE_SERIALIZERS)
            return HookPreservingRecipe(
                wrappedSerializer.read(recipeId, buffer).requireCrafting()
            )
        }

        override fun write(buffer: PacketBuffer, recipe: HookPreservingRecipe) {
            buffer.writeRegistryIdUnsafe(ForgeRegistries.RECIPE_SERIALIZERS, recipe.delegate.serializer)
            @Suppress("UNCHECKED_CAST")
            (recipe.delegate.serializer as IRecipeSerializer<IRecipe<*>>).write(buffer, recipe.delegate)
        }

        private fun IRecipe<*>?.requireCrafting(): ICraftingRecipe {
            return this as? ICraftingRecipe ?: throw JsonSyntaxException("Wrapped recipe for hook_damage_preserving_recipe must be crafting recipe.")
        }
    }

}