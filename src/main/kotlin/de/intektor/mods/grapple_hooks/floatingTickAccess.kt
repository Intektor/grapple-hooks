@file:Mod.EventBusSubscriber(modid = GrappleHooks.ID, bus = Mod.EventBusSubscriber.Bus.FORGE)

package de.intektor.mods.grapple_hooks

import net.minecraft.network.play.ServerPlayNetHandler
import net.minecraftforge.fml.common.Mod

internal var ServerPlayNetHandler.floatingTickCount: Int
    get() = throw UnsupportedOperationException()
    inline set(value) {
        FloatingTickAccess.setFloatingTickCount(this, value)
    }