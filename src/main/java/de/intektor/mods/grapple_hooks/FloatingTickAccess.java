package de.intektor.mods.grapple_hooks;

import net.minecraft.network.play.ServerPlayNetHandler;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;

// written in Java because Kotlin's support for PolymorphicSignature is not all there yet :(
class FloatingTickAccess {

    private FloatingTickAccess() {
    }

    private static final MethodHandle floatingTickCountMh;

    static {
        Field field = ObfuscationReflectionHelper.findField(ServerPlayNetHandler.class, "field_147365_f");
        try {
            floatingTickCountMh = MethodHandles.publicLookup().unreflectSetter(field);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    static void setFloatingTickCount(ServerPlayNetHandler handler, int count) throws Throwable {
        floatingTickCountMh.invokeExact(handler, count);
    }
}
